## Introduction et motivation

Bonjour à toutes et à tous, je vous souhaite la bienvenue sur mon site. Je suis Thibault Leonard, j'ai 20 ans et je suis étudiant en 3eme année d'un bachelier en sciences biologiques. Si dans le cursus de biologie les techniques de fabrication et de réalisation numérique sont peu enseignées, celles-ci m'intéressent tout de même grandement. 
Plus précisément, le choix de cette option fut largement motivé par une envie de longue date, celle de réaliser un projet **interdisciplinaire**. Et par cet intermédiaire, avoir un aperçu des savoirs et savoir-faire acquis par des étudiants dans **d'autres facultés** et de leur apporter les miens.

## Qui suis-je ?

Aussi loin que je me souvienne, j'ai toujours été très **curieux de nature**, c'est ce qui m'a assez naturellement poussé vers un parcours scientifique (académiquement parlant).
Toutefois, en dehors des cours, je passe le plus clair de mon temps à faire de la *musique* (composer et interpréter).
Mon instrument de prédilection est le piano mais en réalité je suis assez ouvert et j'aime toucher un peu à tout.

![](images/pianoresized.jpg)

*L'endroit où je passe le plus de temps*

Lorsque je ne fais pas de musique, je passe également beaucoup de temps sur mon ordinateur pour :
* Enregistrer / utiliser des logiciels d'édition musicale et de composition (DAW)
* Jouer à différents jeux en ligne
* Travailler pour l'université, notamment pour les traitements statistiques en R et la réalisation de rapport

J'ai donc construit **une certaine aisance avec les outils informatiques** (vitesse de frappe, introduction à des langages de programmation, recherche efficace,) au cours de mon parcours.

De manière plus anecdotique, je suis aussi passionné d'histoire (comme le montre la photo qui suit)
![](images/chevalierresized.jpg)

*Moi posant fièrement devant une armure française du XVIème siècle*

## Projets passés et présents

Le long de mon parcours académique j'ai entrepris différents projets, qu'ils fassent **partie intégrante d'un cours ou non**, en voici quelques-uns :
1. **GoL** : En 5ème secondaire j'ai réalisé avec un ami, maintenant en étude d'informatique, une version didactique du célèbre automate "Game of life" de John Conway dans lequel un grand nombre de paramètres était modifiables.

![](images/golresized.png)

*Notre version de Game of Life*

2. **Science épatante**: Petit projet de pédagogie autour de la physique et la chimie entre élèves de 6eme secondaire pour aider les élèves de primaires éprouvant des difficultés en leur présentant les aspects ludiques de la science.
3. **Expérience sur la compétition interspécifique**: Dans le cadre du cours d'écologie, j'ai conduit une expérience de croissance qui a permis d'établir l'intensité de la compétition interspécifique pour les ressources entre le seigle et la moutarde.
4. **Le déclin des arthropodes en Europe**: Dans le cadre du cours d'éthique, j'ai réalisé après l'étude de la controverse sur le déclin des insectes en Europe un débat fictif reprenant les différentes causes et acteurs en jeux.
5. **Embouchure de saxophone en impression 3D**: Etant fasciné par les opportunités offertes par l'impression 3D j'ai entrepris il y a 1 an de modéliser (à partir d'une structure préexistante) une embouchure de saxophone qui répondrait à mes envies en terme de timbre.


#### Mes ambitions et la suite de mon parcours

Très attiré par le domaine de la **génétique humaine**, j'ai pour but actuel d'entreprendre un master de génétique du développement à l'étranger et si possible de travailler dans un département étudiant **les causes des maladies neurodégénératives** (comme le fait le professeur Eric Bellefroid dans son département à l'IBMM).
![](images/ibmm_resized.jpg)

*Institut de Biologie et de Médecine moléculaire de l'ULB à Gosselies*

Toutefois, je continuerai à considérer, jusqu'au moment de ma décision, d'autres options très intéressantes que sont la **bio-informatique** ou la **pharmaceutique**.

J'espère d'ailleurs que cette option au FabLab, en m'en rapprochant le temps d'un quadrimestre, me permettra d'élargir ma vision du monde de l'informatique et de l'ingénierie. Ce qui apporterait à ma réflexion quant à mon choix futur.
