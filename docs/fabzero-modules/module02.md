# Computer-aided design
# 1. Introduction et objectif

À travers ce second module, différents outils de **conception assistée par ordinateur (CAO)** ont été introduits. Ceux-ci se différencient dans leur mode d'utilisation, leur objectif ou le contexte de conception auquel ils sont associés. D'autre part, une notion fondamentale en termes d'éthique et de propriété intellectuelle a été abordée de manière pratique : les licences. Plus précisément, **les licences Creative Commons**, largement rependue et utilisée. 

L'objectif pratique de ce module est la réalisation (conception et impression) d'un petit kit **Flexlinks** servant à la réalisation d'un mécanisme **frugale**. Mais également, l'échange entre étudiant de conceptions dans le respect des conventions de propriété intellectuelle et de droit d'auteur.

# 2. Méthodes

## 2.1 S'informer

Avant de rentrer dans des détails plus techniques, la première chose à faire et de s'informer sur :
1. Les objectifs du module
2. Les principes et techniques à mettre en œuvre pour y parvenir

Les informations sur les objectifs et la théorie sont trouvable sur [la page de Fabzero experiments](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/fabzero-modules/computer-aided-design/), sur [la page gitlab de l'assignement ](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/template/-/issues/2). J'ai récupéré des informations complémentaires à partir de plusieurs sources différentes :
1. [Flexlinks by The BYU Compliant Mechanisms Research Group (CMR)](https://www.compliantmechanisms.byu.edu/flexlinks),  site officiel des Flexlinks qui permet de comprendre le concept et d'observer ce qui a déjà été fait.
2. Très bonne [vidéo de Veritasium sur les compliant mechanisms](https://www.youtube.com/watch?v=97t7Xj_iBv0&ab_channel=Veritasium) recommandée sur la page du module. 
3. [La chaine de mathcodeprint](https://www.youtube.com/@mathcodeprint) qui présente un grand nombre de tutoriel de qualité sur OpenSCAD
4. [La page officielle de Creative Commons](https://creativecommons.org/about/cclicenses/)
5. Le discord du cours et les divers questions réponses entre étudiants

*NB : Il va sans dire que les informations et techniques précisées dans le module 1 ont également contribuer à la réalisation de ce module et à la documentation correspondante*

## 2.2 Outils utilisés

Les outils utilisés, leur types et les liens correspondants de ceux-ci sont ici repris. Les modes d'utilisations respectifs de ceux-ci seront détaillés dans les sections dédiées.


| Nom | Type | Liens | Section dédiée |
|-----|---------|---------------------|----------------|
|OpenSCAD|Logiciel de CAD hautement paramétrique et basé principiellement sur la géométrie.|https://openscad.org/|2.4.1, 2.4.2
*Les outils ci-dessous ont été utilisés dans un but pédagogique (de test) et non directement pour la réalisation de la pièce.*
|Inkscape|Logiciel de conception 2D qui permet le dessin vectoriel (svg)|https://inkscape.org/fr/|2.3.2
|FreeCAD|Logiciel de modélisation paramétrique 3D  gratuit|https://www.freecad.org/|2.4.1

## 2.3 Conception 2D 
### 2.3.1 Format vectoriel vs matriciel

Lorsqu'on traite de conception 2D, deux type de formats s'opposent : le format vectoriel (ex: svg) et le format matriciel (ex: png, jpg, ...). En quelques mots, les images vectorielles sont des images composées de formes géométriques telles que des lignes, des courbes et des polygones. Ces formes sont décrites comme des ensembles de vecteurs qui sont autant de "chemin" déterminant la position, la taille et la forme de chaque élément de l'image. Les images vectorielles sont souvent utilisées pour les graphiques, les logos, les illustrations et les dessins techniques. Elles sont idéales pour les images qui doivent être redimensionnées sans perte de qualité.

Les images matricielles, également appelées images raster, sont composées de pixels, qui sont des points de couleur individuels qui forment une image. Chaque pixel est stocké sous forme de valeur numérique qui détermine sa couleur et sa luminosité. Les images matricielles sont souvent utilisées pour les photographies, les images complexes et les images qui nécessitent des effets de dégradé. Elles sont moins adaptées aux redimensionnements, car chaque pixel doit être agrandi ou réduit, ce qui peut entraîner une perte de qualité.

(Sources : [Adobe](https://www.adobe.com/be_fr/creativecloud/file-types/image/vector/svg-file.html), [UVED](https://www.emse.fr/tice/uved/SIG/Glossaire/co/Raster_format.html))

### 2.3.2 Inkscape

Inkscape est un logiciel libre et gratuit de dessin vectoriel disponible sur les systèmes d'exploitation Windows, Mac et Linux. Il permet de créer toutes sortes d'objets 2D vectoriels tels que des logos, des icônes, des illustrations, des diagrammes, des cartes et des schémas techniques.

Lorsqu'on crée un nouveau document on est assez impressionné par la quantité d'outils disponibles et les possibilités d'édition. Je vais ici présenter 2 manipulations à travers lesquelles j'ai très succinctement tester Inkscape.

![](images/homescreen_resized.png)

1. Après avoir créé mon petit logo, celui-ci est une image matriciel. J'aimerais donc, pour pouvoir le redimensionner plus précisément et faciliter son utilisation, le "rendre" vectoriel. C'est à dire demander à Inkscape les vecteurs qui définissent les formes qui constituent le logo.
![](images/Chemin_resized) 
![](images/crop1_resized) 
Je peux ensuite manipuler ces vecteurs comme bon me semble
![](images/crop2_resized)

2. Je souhaiterais rajouter un petit chapeau à cet œil pour le rendre plus sympathique. Toutefois la forme que je veux n'existe pas dans les formes par défaut. Je vais la créer en soustrayant deux cercles à un rectangle.
![](images/hat1_resized) 
![](images/hat2_resized) 
![](images/hat3_resized)

Voilà qui est fait. Dans l'idée Inkscape permet virtuellement de faire tout et n'importe quoi en ce qui concerne la conception 2D c'est un outil très puissant que je n'oublierai pas d'utiliser si le contexte se présente à nouveau dans mon parcours.



## 2.4 Conception 3D

## 2.4.1 OpenSCAD vs FreeCAD
OpenSCAD et FreeCAD sont deux logiciels de conception assistée par ordinateur (CAD) avec des fonctionnalités différentes et des approches différentes pour créer des modèles 3D.

OpenSCAD est un logiciel de CAD basé sur la géométrie de construction de solide (CSG) et la conception paramétrique. Il utilise un langage de script pour créer des modèles 3D, ce qui signifie que l'utilisateur écrit du code pour décrire la géométrie du modèle. Le code est compilé en une représentation 3D qui peut être visualisée et exportée dans différents formats de fichier. 

FreeCAD, d'autre part, est un logiciel de CAD paramétrique avec une interface utilisateur graphique (GUI). Il est donc en théorie facilitant en termes d'utilisation et offre un plus grand nombre de possibilités pour une conaissance géométrique moindre.

Toutefois, après plusieurs tests, j'ai vite largement préféré l'aspect intuitif et facile à prendre en main de OpenSCAD, le langage est très simple et purement géométrique, les arrangements menant à la pièce finale sont facilement programmables et visualisables. Là où FreeCAD m'a perdu par la très grande quantité d'options et de fenêtre qu'il présente.

## 2.4.2 Réalisation d'une pièce Flexlinks dans OpenSCAD

Pour la réalisation de ma pièce dans OpenSCAD, j'ai procédé comme suit :
##### a. Trouver le modèle de pièce qui allait me servir de base : 
Pour cela je me suis rendu sur le site de Flexlinks et j'ai fini par choisir ce [modèle très utilisé](https://www.thingiverse.com/thing:3020736) qui consiste en deux pièce de fixation reliée par un "câble" flexible. Nous verrons par la suite que j'ai décidé de lui ajouter un élément qui me semble intéressant.

**Remarque importante : l'entièreté du code qui va suivre sera codé de manière paramétrique afin de permettre des tests et un changement aisé des différentes variables qui déterminent les dimensions la pièces.**

##### b. Réaliser la base pour les pièces de fixation :
Je suis parti du modèle et j'ai observé que les extrémités sont déterminées par une union entre un parallélépipède centrale et 2 cylindre identiques. Le module que j'ai créé correspondant à cette forme dans OpenSCAD est le suivant :
```
module roundSquare(radius, length, height){
    cylinder(height,r=radius);
    translate([0,length,0]){
            cylinder(height,r=radius);      
    }
    translate([-radius,0,0]){
    cube([radius*2,length,height]);
    }
}
```
NB : Noter qu'il serait possible d'utiliser une boucle for pour la réalisation des deux cylindres. Toutefois, étant donné leur nombre de 2 j'ai choisi cette option plus lisible au premier regard.

Voici le rendu de ce premier module : 
![](images/roudsquare_resized.png)

##### c. Réaliser le modules des trous :

Les trous consistent en une différence entre les cylindres servant à la base et d'autres cylindres "vides". Par un jeu de translation, il est assez facile de faire correspondre la "hauteur" du cylindre comprise par OpenSCAD en une "profondeur" de trous.
```
module holes(radius, height, depth, length){
    translate([0,0,-depth]){
        translate([0,0,height]){
            cylinder(depth,r=radius);
            translate([0,length,0]){
                cylinder(depth,r=radius);
            }
        }
    }
}
```
On peut alors combiner les modules pour obtenir une pièce de fixation complète :
```
module plugPiece(rext, rint, depth, height, length){
    difference(){
        roundSquare(rext,length,height);
        holes(rint,height,depth,length);
    }
}
```
Voici le rendu de cette pièce :
![](images/plugpiece_resized.png)

##### d. Réaliser le module des câbles :
Rien de très compliqué ici, les câbles sont simplement des parallélépipèdes rectangles très fins et longs. 
```
module cable(height,length,width){
  rotate([0,0,90]){
    cube([width,length,height]);
    }
}
```

##### e. Combiner les modules pour obtenir la pièce finale :
La pièce finale est composée de deux "plugPiece", deux "câbles" et une petite pièce centrale semblable à un lego unitaire.
```
module finalPiece(rext,rint,depth,height, plength, cwidth,clength){
    plugPiece(rext,rint,depth,height,plength);
    translate([0,clength*2+plength+4*rext,0]){
        plugPiece(rext,rint,depth,height,plength);
    }
    translate([0,plength+3*rext+2*clength,0]){
        rotate([0,0,90]){
            cable(height,clength,cwidth);
        }
    }
    translate([0,plength+rext+clength,0]){
        rotate([0,0,90]){
            cable(height,clength, cwidth);
        }
    }
 translate([0,plength+rext*2+clength,0]){
        rotate([0,0,90]){
            difference(){
                cylinder(h=height, r=rext);
                translate([0,0,-depth]){
                    translate([0,0,height]){
                        cylinder(depth,r=rint);
                    }
                }
            }
        }
    }
}
```
Voici le rendu de la pièce finale :
![](images/finalpiece_resized.png)

##### f. Ajouter les paramètres ainsi que la licence Creative Commons en haut du code :
J'ai choisi une licence CC-SA (pour share-alike) ce qui signifie que les personnes qui utiliseront ce code pourront à leur tour le partager sous réserve d'utiliser la même licence.

Licence :
```
/*
FILE : three_part_flexlink.scad

AUTHOR : Thibault Leonard <thibault.leonard@ulb.be>

DERIVED FROM https://www.thingiverse.com/thing:3020736

LICENSE : Creative Commons : Attribution-ShareAlike 4.0 International [CC BY-SA 4.0]
//*/
```
Paramètres de la pièces :
```
//Rayon extérieur des trous
radExt=8;
//Rayon intérieur des trous
radInt=6;
//Profondeur des trous
depth=5;
//Hauteur des pièces
height=10;
//Longueur des pièces aux extrémités
lengthPiece=20;
//Epaisseurdes cables
widthCable=1.5;
//Longueur des cables (individuellement)
lengthCable=75;
//Nombre de facettes (résolution des cylindre)
$fn=120;
```

**Le script complet de la pièce se trouve [ici](https://pastebin.com/2sF8N9RS).**
# 3. Pièces crées par d'autres étudiants

Lors de l'impression, nous avons décidé avec Louis et Victor de combiner nos pièces pour faire une machine. 
Je n'ai pas utilisé leur code mais nous avons directement réunis nos pièces après leur impression.
Toutefois, je les cite tout de même ici:
1. File : Cross-Axis Flexural pivot Flexlink reverse engineering script (https://www.compliantmechanisms.byu.edu/flexlinks)
Author: Victor De Pillecyn
License: Creative Commons (CC) BY
2. File : Flexible lego connector
Author : Louis Devroye
License : Creative Commons, Attribution-ShareAlike 4.0 International


# 4. Check-list

- [X] Design, model, and document a parametric FlexLinks construction kit that you will 
**fabricate next week using 3D printers.**
- [X] Make it parametric, so you will be able to make adjustments accounting for the 
**material properties and the machine characteristic**
- [X] Read about Creative Commons open-source Licence and add a CC license to your work.
- [X] Complete your FlexLinks kit that you made by fetching the code of at least 1 part made by 
**another classmate during this week. Acknowledge its work. Modify its code and get the parts ready to print.**

**La suite se trouve dans le module 3**
