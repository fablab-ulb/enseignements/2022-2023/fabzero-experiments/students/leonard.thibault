# Laser cutting
# 1. Introduction et objectifs

Lors de module qui consistait à suivre une ou plusieurs formations proposées par le fablab sur les différents outils de fabrication numérique, mon choix s'est porté sur la découpe laser. Ce choix a été principalement motivé par les différents objets que j'avais pu déjà voir être conçu sur ce genre de machine et devant lesquels je me posait la question de leur conception.

Les objectifs de cette formation étaient d'une part comprendre et savoir utiliser une machine de laser cutting en toute autonomie et sécurité et d'autre part réalisé des pièces qui utilisent les différentes techniques proposées par ces machines.

# 2. Formation

## 2.1 Commentaires globaux et introduction
La formation fut très complète et instructive. De plus, le fait que nous ne soyons que 4 dans notre groupe a permis un très grand nombre d'échange avec le formateur et une aide précise de sa part sur les différents problèmes que l'on rencontrait lors des premiers tests.

Avant d'accéder aux machines, nous avons réalisé un petit quizz pour vérifier notre bonne connaissance des procédure de sécurité et des différents matériaux utilisable pour les découpes.
Ces règles sont listées sur [la page dédiée du cours](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Laser-cutters.md). 
# 2.2 Tour des machines

Les 3 machines de laser cutting du fablab nous ont été présentées toute avec leur lot d'avantages et d'inconvénients et leur zone de compétence spécifique.
Ce tableau reprend les informations que j'en retire :

| Nom | Type | Puissance | Taille de plateau | Précision | Facilité d'utilisation | Quels projets ? |
|-----|---------|---------------------|----------------|-----|-----|-----|
|Epilog fusion PRO| Machine professionnelle produite par EpilogLaser (USA)|60W|81 x 50 cm| Très précise grâce au focus fait par le palpeur|Très facile d'utilisation grâce au dashboard Epilog lorsqu'on lance l'impression (nécessite clef usb)| Très polyvalent
|Full spectrum Muse|Petite machine pour particulier produite par Full spectrum Laser (USA) |40W|50 x 30 cm|Relativement élevée, focus fait avec des plaquettes standard donc pas parfaitement précis.|La plus facile, il suffit de se connecter au serveur de la machine et placer son dessin| Limite de taille importante mais parfaite pour les projets que j'ai réalisé.
|Lasersaur|Machine DIY opensource construite par le FabLab|100W|120 x 60 cm|Moins précise que les autres dû à une légère inclinaison du plateau|Relatviement difficile, la prise en main est ralentie par le logiciel (interface raspberry pi et la lenteur du PC)|Projets grand en taille et matériau demandeur en puissance.|

# 2.3 Vitesse et puissance : les 2 paramètres importants

La vitesse et la puissance sont deux paramètres importants dans la découpe laser car ils affectent directement la qualité et la précision de la découpe.

La vitesse de déplacement de la tête de découpe laser influe sur la qualité de la découpe en affectant la largeur de la zone affectée par la chaleur et la taille de la zone de fusion. Une vitesse de découpe trop élevée peut entraîner une perte de précision ou des bavures, tandis qu'une vitesse trop lente peut causer une surchauffe et endommager la pièce.

La puissance du faisceau laser détermine la capacité de la machine à couper à travers différents types de matériaux. Une puissance de découpe trop faible peut ne pas être capable de couper à travers des matériaux plus épais ou plus denses, tandis qu'une puissance excessive peut provoquer des brûlures ou des déformations dans la pièce.

Pour déterminer ses deux paramètres il est donc important de connaître le matériel sur lequel on va travailler et son épaisseur. On peut ensuite réaliser une plaques tests (qui copporte différents carrés découpés avec des vitesses et des puissances différentes) ou de trouver une qui a déjà était faite pour le matériau et l'épaisseur.

# 3. Premier essai

Mon premier essai fut sur la Full spectrum Muse en binôme avec Merlin. Nous avons choisi de découpé dans du plexiglas noir 3 pièces permettant d'assembler un cavalier (pièce d'échec). 

![](images/dcc_resized.jpg)

![](images/dcc2_resized.jpg)

Pour déterminer la vitesse et la puissance à utiliser nous avons utilisé la plaque de test en plexiglas de la même épaisseur déjà réalise (3mm)

![](images/testplate1_resized.png)

## 4. Mon kirigami : carte pop-up pour ma cousine

Le 23 mars, jour de la réalisation du kirigami (assignement pour cette formation), c'était l'anniversaire de ma petite cousine. J'ai donc décider d'allier l'utile à l'agréable en réalisant pour elle une carte pop-up avec un petit dinosaure en relief. De plus, étant très mauvais en dessin, je me suis dit que ce serait relativement dans mes cordes à designer dans inkscape. Le concept de carte pop-up rentre dans le concept de kirigami qui est définit comme un art japonais qui consiste à découper et plier du papier pour créer des motifs en relief.

J'ai donc dans un premier temps desinger la carte sur inkscape. Pour cela j'ai d'abord dessiné en noir les parties qui seront découpée : le dinosaure et les languettes par-dessus sa tête et son dos afin de pouvoir le mettre en relief après la découpe. Le contour de la carte est également dessiné en noir afin que je puisse la retirer de la feuille d'origine. Ensuite, j'ai dessiné en rouge ce qui allait être "marquer" : l'écriture "Happy dino-birthday", la date, ainsi que les marques de pliages qui me permettront de plier plus facilement après l'impression.

*NB : J'utilise le terme "marquer" pour signifier un mode découpe mais avec une puissance et une vitesse insuffisante pour passer au travers, là où la gravure est un fonctionnement différent sous forme matriciel avec interruption et rallumage du laser (comme une imprimante à encre)*

![](images/dino_resized.png)

J'ai choisi comme matériau une grande feuille A3 cartonnée de 0,15 mm d'épaisseur. Après m'être concerté avec Axel qui était présent lors de l'impression, il m'a indiqué que même une très faible puissance et une très grande vitesse passerait à travers que je n'avais donc pas réellement besoin de faire tests. Effectivement, la découpe c'est très bien passée du premier coup, j'ai utilisé les paramètres suivant par couleur (toujours sur la muse).

1. Noir : V=85, P=20
2. Rouge : V=100, P=5
3. Bleu : V=100, P=5

Seule petite modification par rapport à mon idée original seul le contour des éléments bleus sont gravés.

Voici le résultat de la découpe après pliage 
![](images/dinof_resized.jpg)





# 3. Check-list

- [X] Demonstrate and describe 2D design processes
- [X] Identify and explain processes involved in using the laser cutter.
- [X] design, lasercut, and document a kirigami (cut and folds) to make a 3D object


# 4. Apprentissages
Comme pour l’impression 3D, j’ai pris beaucoup de plaisir dans l’apprentissage de l’utilisation des machines. C’est une compétence supplémentaire et une façon de plus de projeter dans la réalité une idée que l’on aime. Mais j’en retire une satisfaction supplémentaire dans la mesure où il m’a été possible de réaliser une pièce en toute autonomie. Le fait d’essayer plusieurs fois, sur le matériel que j’ai choisis, de comprendre mes erreurs, de voir la machine dessiner petit à petit le dessin que j’avais créé et modifié et enfin d’avoir dans mes mains un cadeau original pour ma cousine réalisé avec un technique que je venais d’apprendre, c’était une expérience très stimulante.
