# Project management and documentation
# 1. Introduction et objectifs

À travers ce premier module, les principes et techniques déterminants pour **une bonne documentation et une gestion correcte de projet** ont été présentés. Cet assignement constitue un exercice de première application de ceux-ci qui approfondis lors de la documentation du projet final. De plus, ces techniques sont autant d'outils qui pourront servir dans la suite de nos études ou notre carrière.

**Préambule important**
*Du fait d'un conflit horaire majeur avec des cours en laboratoires, je n'ai pas pu assister en présentiel aux séances de théorie et d'exercices qui portaient sur ce module. Toutefois, après avoir visionné l'enregistrement du cours et lu les informations disponibles sur la page Fabzero j'ai pu saisir l'importance et la raison d'utilisation des différents principes et techniques abordées (Markdown, gitlab (remote repo), importance de la documentation,). J'aimerais tout de même souligner **l'aide précieuse** que mon binôme de soutien, Louis Devroye, m'a apporté pour la compréhension de points plus particuliers (Clef SSH par exemple).*

# 2. Méthodes

## 2.1 S'informer

Avant de rentrer dans des détails plus techniques, la première chose à faire et de s'informer sur :
1. Les objectifs du module
2. Les principes et techniques à mettre en œuvre pour y parvenir

Les informations sur les objectifs et la théorie sont trouvable sur [la page de Fabzero experiments](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/fabzero-modules/project-management-documentation/), sur [la page gitlab de l'assignement ](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/template/-/issues/),ainsi que dans l'enregistrement du cours correspondant. J'ai récupéré des informations complémentaires, souvent plus techniques, à partir de plusieurs sources différentes :
1. [Le Markdown Guide](https://www.markdownguide.org/), un tutoriel très complet sur l'utilisation et la syntaxe du langage
2. [La documentation de gitlab sur les clefs SSH](https://docs.gitlab.com/ee/user/ssh.html)
3. [La documentation git](https://git-scm.com/doc)
4. Le discord du cours et les divers questions réponses entre étudiants


## 2.2 Outils utilisés

Les outils utilisés, leur types et les liens correspondants de ceux-ci sont ici repris. Les modes d'utilisations respectifs de ceux-ci seront détaillés dans les sections dédiées.


| Nom | Type | Liens | Section dédiée |
|-----|---------|---------------------|----------------|
|gitbash|CLI (command line interface)|https://git-scm.com/downloads|2.3
|gitlab|Remote repository|https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/leonard.thibault|2.3, 2.4
|Imagemagick|Edition photo|https://imagemagick.org/script/download.php|2.5
|Dillinger|Editeur markdown en ligne|dillinger.io|2.4

## 2.3 CLI

Une CLI ou "command line interface" permet la communication entre l'utilisateur et le système par l'intermédiaire de suites de ligne de commande qui permettent la réalisation d'actions successives. Ici, les actions qui nous intéressent sont les suivantes :
1. Créer une clef SSH qui nous permettra une connexion et une communication sécurisée avec le repository gitlab
2. Mettre à jour le repository gitlab suite aux modifications locales sur les travaux en cours

### 2.3.1 Clef SSH

Les clefs SSH font partie intégrante d'un protocole de sécurisation de la communication informatique largement rependu : le protocole SSH. Les clefs sont des suites de caractères prenant différente forme et fonctionnant par paire.
1. La clef public sert au cryptage des informations et est utilisée par le système de l'utilisateur et le système à distance (remote)
2. La clef privée est propre au système de l'utilisateur et sert d'identifiant lors de la communication avec le système à distance. Elle est caractérisée par un phrase de passe (passphrase) qu'il faut absolument maintenir secrète.
([Source](https://jumpcloud.com/blog/what-are-ssh-keys))

Sur Windows il est possible de générer une paire de clef SSH, pour cela je suis passé par [le protocole suivant](https://docs.gitlab.com/ee/user/ssh.html). En pratique, j'ai tapé la ligne de code suivante dans le CLI.
```
ssh-keygen -t ed25519 -C "<comment>"  
```
*(Avec pour "comment" mon adresse e-mail)*
Il faut ensuite spécifier la phrase de passe, récupérer les clefs générées (privées et public) et ajouter la clef public dans gitlab sur [la page suivante](https://gitlab.com/-/profile/keys). La clef est alors connue par gitlab et une communication sécurisée est possible.

### 2.3.2 Communication et mise à jour du repository

Les schéma suivant illustre ce qui sera expliqué pratiquement ici : 
![](images/reporesized.png)

La première étape de ce processus est la création d'un repository sur le système utilisateur qui permettra de travailler en local sur le projet concerné. Pour cela nous allons cloner le repository présent sur gitlab. 
La première étape est de spécifié les identifiants gitlab afin d'accéder à l'archive.
```
git config --global user.name "your_username"
git config --global user.email "your_email_address@example.com"
git config --global --list
```
*(Cette dernière ligne permet de vérifier si les identifiants ont été correctement spécifiées et intégrées)*
Une fois ces identifiants spécifiés nous pouvons procéder au clonage
```
cd (chemin d'accès du repository local)
git clone (lien du remote repository)
```
![](images/linkresized.png)

*(Le lien a spécifié dans la commande est trouvable ici)*

Le clonage étant effectué, il est maintenant possible de travailler en local. Après avoir travaillé et effectuer des modifications satisfaisantes sur le projet, il faut mettre à jour le remote repository pour cela on va procéder en 3 étapes (voir schéma).

La première chose à faire est de constater les modifications pour cela la commande utilisée est :
```
git status
```
En suite, dans l'ordre,
```
git add chemindufichier
git commit -m "commentaire"
git push
```
permettent, comme spécifier dans le schéma, le processus de mise à jour du remote repository. Attention, gitlab garde en mémoire l'entièreté des versions antérieures d'un projet, et ce, à long terme, il faut donc être parcimonieux et vérifier son travail avant de l'envoyer sur le remote repository pour ne pas alourdir inutilement celui-ci.

## 2.4 Markdown

Pratiquement, pour travailler sur la réalisation des différentes pages des modules le langage Markdown est utilisé. Il s'agit d'un langage dit de balisage ce qui signifie qu'il permet de spécifier une mise en page par l'intermédiaire d'une syntaxe spécifique. En opposition aux applications WYSIWYG (what you see is what you get) qui proposent une mise en format en temps réel par l'intermédiaire de boutons et de raccourcis du texte tapé. Le contenu d'un fichier markdown n'est donc pas identique au rendu observé sur la page mais détermine celui-ci.

Plusieurs environnements permettent l'édition Markdown (notamment Visual studio code, le plus connu) mais pour ce travail j'ai choisi [dillinger](dillinger.io), un éditeur en ligne relativement populaire qui permet la visualisation en temps réel du rendu final par l'intermédiaire d'une seconde fenêtre. C'est donc très pratique lorsqu'on découvre le langage est que l'on n'est pas encore très à l'aise avec la syntaxe. 

![](images/dilliresized.png)

Pour la syntaxe à proprement parler, j'ai cumulé les informations présentes dans le cours ainsi que sur la ["Cheat sheet"](https://www.markdownguide.org/cheat-sheet/) du Markdown guide. Après quelques heures d'acclimatation, le langage s'est révélé être très pratique et facile d'utilisation, une bonne découverte !

## 2.5 Traitements et insertions d'images

Afin de ne pas alourdir inutilement le repository gitlab, il est intéressant de compresser les images utilisées et ainsi diminuer leur poids. J'ai donc considérablement réduit le poids de chaque image par l'intermédiaire de l'application Imagemagick, une application très simple d'édition d'images. 

![](images/imagemagickresized.png)

Pour les insérer dans le fichier md, il suffit ensuite d'utiliser la syntaxe suivante :
```
![légende](image.format)
```
# 3. Check-list

- [X] Make a website and describe how you did it
- [X] Introduced yourself
- [X] Documented steps for uploading files to the archive
- [X] Documented steps for compressing images and keeping storage space low
- [X] Documented how you are going to use project management principles 
- [X] Pushed to the class archive
# 4. Conclusions et apprentissages

Je retire un grand nombre d'apprentissages de ce module, qu'il soit simplement techniques (en témoigne les outils utilisés pour ce travail) ou conceptuels ([Project managment principles](https://gitlab.com/fablab-ulb/enseignements/fabzero/basics/-/blob/main/project-management-principles.md)). Toutefois, je ne mets pas suffisamment encore ces derniers en applications, plus précisément le As-you-work. En effet, je dois progresser dans ma régularité et ma discipline de travail et moins fonctionner par pics de productivité. D'un point de vue plus technique, je suis sûr que Markdown et git présentent encore un grand nombre de possibilités que je n'ai pas encore eu l'occasion de saisir mais les travaux futurs ne feront que m'y plonger davantage.
