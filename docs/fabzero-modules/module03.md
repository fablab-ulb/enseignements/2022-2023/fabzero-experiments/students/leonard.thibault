# 3D printing
# 1. Introduction et objectifs

Ce module fait suite au précédent dans la mesure où il traite de l'impression 3D des pièces designées dans le module 2.
Cette impression "test" a suivi une formation à l'impression 3D qui a pour but de permettre l'impression en autonomie au fablab pour la suite du cours.

# 2. Méthodes

## 2.1 S'informer

Avant de rentrer dans des détails plus techniques, la première chose à faire et de s'informer sur :
* Les objectifs du module
* Les principes et techniques à mettre en œuvre pour y parvenir

Les informations sur les objectifs et la théorie sont trouvable sur [la page de Fabzero experiments](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/fabzero-modules/3d-printing/), sur [la page gitlab de l'assignement ](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/template/-/issues/3). Dans le [tutoriel Prusa Slicer](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md) ainsi que sur [la page officiel Prusa](https://help.prusa3d.com/fr/category/utilisation-de-limprimante_202#_ga=2.91883037.682718985.1678303686-1925753907.1678303685).


## 2.2 Prusa slicer

PrusaSlicer est un logiciel de découpage (slicing) pour l'impression 3D, développé par la société tchèque Prusa Research. Il s'agit d'un logiciel libre et gratuit, qui permet de préparer les modèles 3D pour l'impression en générant les instructions de découpage (gcode) que l'imprimante 3D utilise pour fabriquer l'objet.

Le logiciel permet de personnaliser les paramètres d'impression tels que l'épaisseur de couche, la densité de remplissage, la vitesse d'impression, la température d'extrusion, la rétractation, etc. pour obtenir des résultats optimaux. Il offre également des fonctionnalités avancées telles que la génération de supports personnalisés pour les parties qui ont besoin de soutien pendant l'impression, la mise en file d'attente de plusieurs modèles d'impression, la simulation d'impression pour prévisualiser le résultat final, et bien plus encore.

Les imprimantes utilisées sont des imprimantes Prusa MK3 / MK3+, le principe des imprimantes Prusa sont qu'elles sont autoréplicatives (c'est à dire que l'on peut imprimer les différents composants matériels de la machine).

![](images/mainscreen_prusaslicer.png)
## 2.3 Préparation du fichier

En pratique pour obtenir un fichier imprimable (gcode), il faut procéder comme suit :

1. Récupération fichier stl à partir de FreeCAD ou OpenSCAD (voir mod02)
2. Import du fichier sur PrusaSlicer
3. Centrer l'objet et s'assurer que la plus grande surface est en contact avec le plateau
4. Réaliser les réglages d'impression (filament, imprimantes, supports, épaisseur de couche, ...)
5. Découper l'objet, on peut alors observer les différentes couches telles qu'elles vont être imprimées
6. Exportation du fichier gcode
7. Placer ce fichier sur une carte SD qui pourra être utilisée par l'imprimante


## 2.4 Paramétrages de la pièce

Pour que les pièces soient compatibles entre elles, il faut d'abord des paramètres qui correspondent effectivement aux réalités physiques de compatibilité. Les paramètres testés sont :
1. Le diamètre des trous intérieur, par l'intermédiaire d'une barrette reprenant plusieurs diamètres 

![](images/testdiameter.jpg) 

2. Le diamètre et la hauteur des "studs" par l'intermédiaire de petite pièces semblables à des lego unitaires de différentes tailles 

![](images/teststud.jpg) 
 
3. L'écartement entre les trous -> +- 8

4. La flexibilité des "cables" par l'intermédiaire de plusieurs pièces de différentes longueurs et épaisseur 

![](images/testflex.jpg) 

J'ai repris dans le tableau ci-dessous les caractéristiques de compatibilités déterminées à partir de ces tests

| Dimension | Valeurs |
|-----|---------|
|Diamètre intérieur| 5-5,3 (en fonction de la facilité de déconstruction souhaitée)|
|Diamètre des studs|4,8-5 (en fonction de la facilité de déconstruction souhaitée)|
|Hauteur des studes|1,8|
|Ecartement des trous|8|
|Longueur et épaisseur pour une flexibilité optimale|1,5x1,5|

# 3. Impression de la pièce

Une fois la pièce correctement paramétrée on peut alors l'imprimée, l'impression de ma pièce a pris 10 minutes. J'ai dû l'imprimer 2 fois étant donné que je m'étais trompé dans la longueur des "câbles" lors de la première.

Voici le rendu final de ma pièce : 

![](images/piecethib.jpg)

Voici les autre pièces imprimées (pièces de Louis Devroye et de Victor De Pillecyn):

![](images/piecelouis.jpg) ![](images/piecevictor.jpg)

# 3.1 Difficultés rencontrées

Nous n'avons pas rencontrés de difficultés particulières lors de l'impression si ce n'est pour la première impression de la pièce de Louis Devroye qui n'a pas réellement fonctionné étant donné la toute petite taille de la pièce imprimée (ensemble brouillon de filament).


# 4. Mécanisme

Pour être honnête, la réalisation de notre mécanisme fut relativement "improvisée sur le moment". Nous nous sommes appelés avec Victor et Louis pour tenter de trouver un petit mécanisme qui ferait intervenir nos 3 pièces.
Nous avons fait un petit "support" pour clef qui vibre lorsqu'on place la clef dessus. Nous n'avons pas réellement trouvé d'utilité pour celle-ci mais les 3 pièces remplissent leurs rôles : 
1. La pièce de Louis permet d'attacher facilement la clef au reste de la structure
2. Ma pièce sert de bras qui permet d'éloigner la clef du socle pour augmenter l'amplitude de vibration
3. La pièce de Victor permet la vibration par sa flexibilité latérale.

![](images/mec1_resized.jpg) ![](images/mec2_resized.jpg)

# 5. Checklist

- [X] In group, test the design rules, comment on the choice of materials and good practices for your 3D printer(s) 
**regarding the kit parts you are going to make.**
- [X] In group, test and set the parameters of your flexible part so they can be combined with other classmate parts.
- [X] Individually, 3D print your flexible construction kit you made.
- [X] In group, make a compliant mechanism out of it with several hinges that do something.

# 6. Apprentissages
J’avais déjà utilisé une imprimante 3D mais j’avais simplement imprimé un fichier que j’avais trouvé intéressant sur internet (embouchure de saxophone). Ensuite je l’avais légèrement modifié pour augmenter la largeur de l’embouchure ou l’épaisseur de la fin. Je n’avais donc pas réellement utilisé les outils du logiciel (Fusion360° à l’époque). De plus, l’imprimante appartenait à quelqu’un d’autre et c’était cette personne qui faisait les réglages pour moi sans plus d’explications. Il va donc sans dire que designée cette pièce m’a apporté un certain savoir-faire qui m’était étranger. De plus, à la suite de cette formation, je me rends compte avoir largement sous-estimé la rigueur demandée pour la compatibilité des pièces (tests préalables, modularité et paramétrage du code). Un apprentissage annexe qui vaut pour la fabrication et l’expérimentation au sens large.
