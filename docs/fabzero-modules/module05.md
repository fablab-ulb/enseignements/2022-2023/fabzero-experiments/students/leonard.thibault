# Team dynamics and final project
# 1. Introduction et objectif

Dans ce cinquième module, nous verrons les méthodes employées pour la formation des groupes pour le projet final, la prise de décision ainsi qu'une façon efficace de définir une problématique, ses causes et ses potentielles solutions.

# 2. Formation des groupes

## 2.1 Objet-problématique

Pour préparer la formation des groupes il nous a été demandé d'apporter un objet qui représentait une problématique qui nous intéresse. Pour ma part j'ai choisi un microscope pour enfant que j'ai reçu de ma grand-mère étant petit. A travers cet objet j'ai amené les problématiques de l'accès à l'éducation et de l'intérêt pour les sciences. De mon expérience personnelle en tant que professeur particulier de sciences (job étudiant) j'ai remarqué que les élèves présents dans mes cours étaient très différents et était loin d'avoir des conditions d'apprentissage similaire. Que ce soit leur environnement socio-économique, leur éducation ou même les opportunités qui s'offraient face à leur curiosité, aucune de ces choses n'étaient égale parmi eux. De plus, je suis convaincu qu'un apprentissage théorique et pratique des sciences apportent énormément aux enfants dans leur compréhension du monde et à leur esprit critique.

## 2.2 Qui se ressemble s'assemble

Pour former un groupe nous nous sommes ensuite réunis entre étudiants en fonction du type d'objet amené, voici ci-dessous la liste des étudiants en fonction de leur objets, problématiques et faculté.


| Nom | Objet | Problématique | Faculté |
|-----|---------|---------------------|----------------|
|Cédric Luppens|Craie|Accès à l'éducation et inégalités|Polytechnique (MA2-BIOMED)
|Stanislas Mondesir|Cable ethernet|Accès à internet dans le monde (plus précisément dans le monde de l'éducation)|Sciences informatiques (BA3)|
|Kamel Sanou|Loupe|Inégalités et pénibilité dans le monde ouvrirer| Bioingénieur (BA3)
|Thibault Leonard|Microscope pour enfant|Accès à l'éducation et aux sciences|Biologie (BA3)

## 2.3 Thématique commune 

Pour trouver la thématique commune aux problématiques que nous abordions, il a d'abord fallu mettre à plat toutes les idées reliées à nos objets. Nous avons ensuite discuté des différentes idées et nous nous sommes mis d'accord sur une thématique commune : l'accès à l'éducation. 
Voici le panneau reprenant les idées globales

![](images/t1_resized.jpg)

Des idées plus précises

![](images/t2_resized.jpg)

Le choix (très unanime...)

![](images/t3_resized.jpg)

## 2.4 Problématique commune 

Pour tenter de tirer le maximum de la thématique, nous avons utilisé la méthode "Moi à ta place je" qui consiste à commencer tour à tour une phrase par cette formule puis exprimer un problématique non-citée liée à la thématique. Nous avons aussi récolté des idées auprès d'autres groupes.

Voici la liste de ces problématiques, celle qui présente le petit sigle vert sont celles pour laquelle une solution technologique semble le plus évident. Les autres sont plus générales / sociales.

![](images/t4_resized.jpg)

# 3. Outils de structure et de prise de décision

## 3.1. L'Art de décider

Il s'agit d'une grille de lecture qui part du principe que 3 types de décisions sont possibles lorsque nous sommes face à un questionnement.

1. Décider seule : on délègue la tâche de la décision selon certain critère à une personne. Ces critères peuvent être le niveau de compétence dans le domaine en question, le temps disponible ou même simplement la motivation. Le groupe place alors sa confiance dans le choix futur de cette personne. 

Dérive : si on a tendance à sur-déléguer, il est aisé de se retrouver dans des situations où plus personne n'a de vision d'ensemble du projet. La coordination devient alors difficile

2. Décider collectivement : on se rassemble pour prendre une décision qui nous semble importante. Pour cela il existe de nombreuses méthodes (consensus, vote, jugement majoritaire, classement, température check, choix sans objection,...). 

Dérive : les groupes qui ont tendance à choisir collectivement pour tout ralentisse grandement leur progression dans la réalisation du projet. Il est important de savoir déléguer et de s'appuyer sur les points forts de chacun. 

3. Ne pas décider : lorsque la quantité d'informations dont on dispose sur un sujet est insuffisante ou que le sujet est largement moins important que le reste. Il est souvent intéressant de reporter la décision et de rassembler des arguments/infos complémentaires. Le maintien de ce sujet à l'ordre du jour des réunions peut créer des tensions et faire perdre du temps. D'autre part, prendre une décision activement est également une perte de temps dans la mesure où le groupe reviendra souvent sur celle-ci.

Dérive : Vouloir éviter à tout prix les tensions en éliminant un grand nombre de prises de décision est souvent néfaste. De plus, s’il faut être sûr qu'une décision soit mature avant de la prendre, il ne faut pas non plus attendre de connaître le moindre détail du moindre aspect de la chose (surtout en situation de deadline imminente).
## 3.2 Température check

Le température check est une technique de décision rapide qui consiste à demander à toutes les personnes du groupe de prendre une position avec leur main / bras simultanément. La position indique le niveau de motivation ou de préférence envers la décision en question (du haut vers le bas). Il est alors très facile de voir le niveau global de motivation au sein du groupe mais également de repérer les éventuelles objections ou "veto" qu'il est nécessaire d'aborder et de comprendre.

Dans notre groupe, nous avons choisis cette méthode comme méthode principale de décision pour les choix stratégiques mineurs (à comprendre tous les choix concernant la direction du projet mais pas la problématique ni la solution en elle-même). Nous l'utilisons également pour les choix opérationnels qui doivent être réglé collectivement

## 3.3 Rôle de réunions

Lorsqu'on considère les rôles qui permettent de déléguer le travail au sein d'un groupe on pense directement aux tâches liées à la réalisation du projet au sens strict. Toutefois, lors de la séance avec Chloé, nous avons appris qu'il est très intéressant de définir des rôles de réunions. Voici ceux qui nous semblaient pertinent.

L'Animateur : Son rôle est de donner l'ordre du jour, de veiller à ce que celui-ci soit bien suivi et que les questions importantes soient abordées voire résolues. 
Dans notre groupe, il est également responsable du temps de parole et doit la redistribuer / la faire tourner si quelqu’un se perd dans son idée ou monopolise la discussion.
L'animateur évite également des disgressions trop longues.

Le secrétaire : son rôle est de prendre note de ce qui est dit lors de la réunion. Dans notre groupe, ce rôle nous semblait être pertinent uniquement lors des rassemblements "irl" (in real life). En effet, lorsque nous nous appelons sur Discord, nous écrivons de notre côté ce que nous devons retirer de la réunion.


# 4. Problem-Solution tree analysis
Cette technique permet d’établir rapidement une « carte » schématique d’un problème, ses causes et conséquences. Ensuite, en partant de cette vue d’ensemble on peut définir sur un schéma semblable une solution en visant un point (généralement une des causes) sur lequel agir.
J’ai réaliser ici l’analyse pour [open PCR]( https://openpcr.org/) un projet de thermocycleur opensource qui répond d’un seul coup à plusieurs problématiques qui entoure les PCR et les analyse en laboratoires de manière générale :

## 4.1 Problem tree
![](images/arbre1_resized.png)

## 4.2 Solution tree
![](images/arbre2_resized.png)
